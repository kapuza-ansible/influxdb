# InfluxDB role
## Use example
```bash
# clone repo
git clone git@gitlab.com:kapuza-ansible/influxdb.git
cd influxdb

# come to example
cd example

# Edit inventory/hosts
vim inventory/hosts

mkdir inventory/group_vars
cp ../defaults/main.yml inventory/group_vars/install_influxdb.yml
cp ../defaults/main.yml inventory/group_vars/setup_influxdb.yml
cp ../defaults/main.yml inventory/group_vars/setup_telegraf.yml

# Edit configs
vim inventory/group_vars/install_influxdb.yml
vim inventory/group_vars/setup_influxdb.yml
vim inventory/group_vars/setup_telegraf.yml

# Install influxdb server
ansible-playbook ./install_influxdb.yml

# Setup and start influxdb
ansible-playbook ./setup_influxdb.yml

# Install telegraf client
ansible-playbook ./install_telegraf.yml

# Setup and start telegraf client
ansible-playbook ./setup_telegraf.yml
```
